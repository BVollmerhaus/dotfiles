#   __ _    _
#  / _(_)__| |_
# |  _| (_-< ' \
# |_| |_/__/_||_|
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

# Add local package directories to PATH
set -gx PATH ~/.local/bin $PATH           # pip
set -gx PATH ~/.gem/ruby/2.7.0/bin $PATH  # RubyGems

bind \ec 'commandline -f repaint'
function fish_right_prompt
    if functions -q _fish_some_chars_prompt
        _fish_some_chars_prompt
    end
end

function bootstrap_fisher
    if not ping -c 1 -W 1 github.com >/dev/null 2>&1
        printf "%sNo connectivity%s - skipping Fisher installation.\n" \
          (set_color -o yellow) (set_color normal)
        return 1
    end

    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

if not functions -q fisher
    bootstrap_fisher
end
